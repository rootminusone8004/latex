\documentclass{beamer}
\usepackage{multirow}
\usepackage{listings}
\usepackage{tcolorbox}
\usepackage{xcolor}
\usepackage{array}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{changepage}
\usepackage{subcaption}
\usepackage[ruled,linesnumbered,vlined]{algorithm2e}
\usepackage[backend=biber]{biblatex}
\addbibresource{uni.bib}
\renewcommand*{\bibfont}{\tiny}
\usepackage{tikz, pgfplots}
\usetikzlibrary{positioning}
\setbeamertemplate{navigation symbols}{}

% Set the theme
\usetheme{Madrid}

% Define the footer template
\setbeamertemplate{footline}{%
    \leavevmode%
    \begin{beamercolorbox}[wd=\paperwidth,ht=2.25ex,dp=1ex]{author in head/foot}%
        \hspace*{0.02\paperwidth}% Adjust the left margin
        \insertdate\hfill\inserttitle\hfill\insertframenumber{} / \inserttotalframenumber\hspace*{0.02\paperwidth}% Adjust the right margin
    \end{beamercolorbox}%
}

% Define style for the file content
\lstdefinestyle{notepad}{
    language=,
    basicstyle=\ttfamily,
    backgroundcolor=\color{white},
    frame=shadowbox,
    rulecolor=\color{black},
    breaklines=true,
    columns=fullflexible
}

% Define custom colors
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\definecolor{bordercolour}{rgb}{0.0,0.0,0.0}
\definecolor{linkblue}{RGB}{0,0,255}

\title{Git Management}

\begin{document}

\author{Md. Hashibur Rahman (Kwoshik)}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
    \frametitle{Presentation Outline}
    \begin{itemize}
        \item Introduction
        \item Installation
        \item Basic Works
        \item SSH Integration
        \item Vscode Integration
        \item Common Files
        \item Git Branch
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \begin{itemize}
        \item Modern software projects face the following challenges:
            \begin{itemize}
            \item Code Management
            \item Collaboration
            \item Version controlling
            \item Work Tracking
            \end{itemize}
            Git can mitigate all of these problems.
        \item Git is a distributed version control system that tracks changes in any set of computer files, usually used for coordinating work among programmers who are collaboratively developing source code during software development.
        \item Git can also be used by single developer during project management as well. 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Installation}
    \textbf{For Linux:}
        \begin{itemize}
        \item Arch Linux based distributions:
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{sudo pacman -S git}
        \end{tcolorbox}

        \item Debian based distributions:
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{sudo apt install git}
        \end{tcolorbox}
        \end{itemize}
    \textbf{For Windows:}
        \begin{itemize}
            \item You can download git bash from \href{https://git-scm.com/downloads}{\textcolor{linkblue}{here}}.
        \end{itemize}
    \textbf{For Mac OS:}
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{brew install git}
        \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Installation (Cont'd)}
    \textbf{Installation steps for Windows:}\\
        After downloading git bash, make sure of these settings in installation process:
        \begin{itemize}
            \item Additional Desktop icon
            \item Use Notepad as Git's default editor
            \item Let Git decide default branch name
            \item Git from command line and also from 3rd party software
            \item Use bundled OpenSSH
            \item Use the OpenSSH library
            \item Checkout Windows-style, commit Unix-style line endings
            \item Use MinTTY
            \item git pull => rebase
            \item Git Credential Manager
            \item Enable file system caching
        \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic Works \hspace{5.2cm} Project initial setup}
    \begin{itemize}
        \item Initialize git repository.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git init}
        \end{tcolorbox}
        \item Change the default branch name to ``main".
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git branch -M main}
        \end{tcolorbox}
        \item Add the remote repository.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \begin{minipage}{\linewidth}
                git remote add origin \\
                "https://github.com/rootminusone8004/demorepo"
            \end{minipage}
        \end{tcolorbox}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic Works (Cont'd) \hspace{3.3cm} Project initial setup}
    \begin{itemize}
        \item Give the git credentials.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.85\textwidth]
            \begin{minipage}{\linewidth}
                \$ git config user.name "rootminusone8004" \\
                \$ git config user.email "rootminusone8004@gmail.com"
            \end{minipage}
        \end{tcolorbox}
        \item Check the overall configuration.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git config --list}
        \end{tcolorbox}
        \item If you messed up anywhere, you can always modify \underline{.git/config} file which is in the project directory.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic Works (Cont'd) \hspace{3.3cm} Regular operations}\
    \vskip -0.5cm
    \begin{itemize}
        \item Check the status by typing the following command
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git status}
        \end{tcolorbox}
        \item The red files are in the working directory (not taken in the consideration yet).
    \end{itemize}
    \begin{figure}[b]
        \centering
        \includegraphics[width=0.75\textwidth]{images/windows/1_red.png}
        \caption{Untracked files}
        \label{f1}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Basic Works (Cont'd) \hspace{3.3cm} Regular operations}
    \begin{itemize}
        \item Add all the files you want to commit with git.
        \begin{itemize}
            \item If you want to add one by one individually:
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git add file1.ext file2.ext file3.ext}
            \end{tcolorbox}
            \item If you want to add all at once:
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git add .}
            \end{tcolorbox}
        \end{itemize}
        \item You can also check the logs.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git log}
        \end{tcolorbox}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic Works (Cont'd) \hspace{3.3cm} Regular operations}\
    \begin{itemize}
        \item The green files are in the staging area.
    \end{itemize}
    \begin{figure}[b]
        \centering
        \includegraphics[width=0.8\textwidth]{images/windows/2_green.png}
        \caption{Staged files}
        \label{f2}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Basic Works (Cont'd) \hspace{3.3cm} Regular operations}
    \begin{itemize}
        \item Make the change confirmed by adding a meaningful message.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git commit -m "v 0.0"}
        \end{tcolorbox}
        \item Push all your changes to the global repository.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git push origin main}
        \end{tcolorbox}
        \item Know more about git.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{git -h}
        \end{tcolorbox}
        \item To learn in depth, download \href{https://git-scm.com/book/en/v2}{\textcolor{linkblue}{this book}}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{SSH Integration}\
    Everytime we make a commit, credentials like username and password need to be given which is bothersome. That's why we need ssh integration.
    \begin{itemize}
        \item \textbf{Installation:}\\
            \begin{itemize}
            \item \textbf{For Linux:}
            \item Arch Linux based distributions:
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{sudo pacman -S openssh}
            \end{tcolorbox}

            \item Debian based distributions:
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{sudo apt install openssh-client openssh-server}
            \end{tcolorbox}
            \item \textbf{For windows:}
            ssh comes pre-installed with git bash. Therefore, all ssh commands need to written in \textbf{git bash}.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{SSH Integration (Cont'd)}\
    \vskip -0.6cm
    \begin{itemize}
        \item \textbf{SSH full path:}\\
        \begin{itemize}
            \item for Linux: \texttt{.ssh}
            \item for Windows: \texttt{/c/Users/rootkit/.ssh}
        \end{itemize}
        \item \textbf{Create ssh key pairs:}\\
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            % \texttt{ssh-keygen -t ed25519 -C "git with ssh"}
            \begin{minipage}{\linewidth}
                \$ cd .ssh \\
                \$ ssh-keygen -t ed25519 -C ``git with ssh"
            \end{minipage}
        \end{tcolorbox}
        Give it a name. Say {\textbf{github}}.
        
        At this moment, you have a public key named {\textbf{github.pub}} and a private key {\textbf{github}}.
        
        Now print the public key and put it in your remote git server.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{cat github.pub}
        \end{tcolorbox}
        \textbf{DO NOT SHARE YOUR PRIVATE KEY TO ANYONE!}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{SSH Integration (Cont'd)}\
    \vskip -0.5cm
    \begin{itemize}
        \item \textbf{Create a config file:}\\
        You need a config file for better management.
        \begin{lstlisting}[style=notepad]
Host github
    Hostname github.com
    User git
    IdentityFile ~/.ssh/github
        \end{lstlisting}

        You also need to give proper permissions just in case.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            % \texttt{chmod 600 .ssh/config}
            \begin{minipage}{\linewidth}
                \$ chmod 600 .ssh/config \\
                \$ chmod 600 .ssh/github \\
                \$ chmod 664 .ssh/github.pub
            \end{minipage}
        \end{tcolorbox}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{SSH Integration (Cont'd)}\
    \vskip -1cm
    \begin{itemize}
        \item \textbf{Verification:}\\
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \texttt{ssh -T git@github}
        \end{tcolorbox}
        If it gives a positive answer, then our operation is successful.\\
        \vspace{10pt}
        You may also need to add that it in case it doesn't work.
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            ssh-add ~/.ssh/github
        \end{tcolorbox}
        \vspace{10pt}
        \item \textbf{HTTPS and SSH git link difference:}\\
        \underline{HTTPS Link}: {\textcolor{codepurple}{https://github.com/rootminusone8004/demorepo}}
        \underline{SSH Link}: {\textcolor{codepurple}{git@github:rootminusone8004/demorepo.git}}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Vscode Integration \hspace{4.1cm} Regular operations}\
    \vskip -0.5cm
    \begin{itemize}
        \item Open the directory with vscode.\\
        In Windows, Open the git bash in the directory and type:
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            code .
        \end{tcolorbox}
        Close the git bash.
        \item Tap on the git button on the right side.
        \begin{itemize}
            \item Number maybe below it. Indicating the changes performed.
        \end{itemize}
        \item Right panel will show the changes.
        \item Tap on the `+' button beside the files. The files will be staged.
        \item Write your commit message in the box.
        \begin{itemize}
            \item Multiline commit is also possible.
        \end{itemize}
        \item Tap on the big blue button to commit
        \item ``Sync Changes'' button will appear. Tap on it.
        \item Confirm your action and done!
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Vscode Integration (Cont'd) \hspace{2.1cm} Regular operations}\
    \begin{figure}[b]
        \centering
        \includegraphics[height=0.7\textheight]{images/windows/3_vscode2.png}
        \caption{git in vscode}
        \label{f3}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Common Files}\
    These files are present in all branches of a standard git repository.
    
    (We will discuss about branches later.)\\
    \vspace{10pt}
    These files are the following:
    \begin{itemize}
        \item .gitignore
        \item INSTALL.md / INSTALL.org
        \item README.md / README.org
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Common Files (Cont'd) \hspace{5cm} .gitignore}\
    A configuration file used to specify intentional untracked files that Git should ignore
    
    \begin{lstlisting}[style=notepad]
# Ignore files generated by IDEs or editors
.vscode/
.idea/
*.suo
*.ntvs*
/test.py
*.sln
*.sw?

# ignore cache files
__pycache__/
/bin/
/dist/
    \end{lstlisting}
\end{frame}

\begin{frame}
    \frametitle{Common Files (Cont'd) \hspace{5cm} .gitignore}\
    \begin{itemize}
        \item Comments start with write {\textcolor{codepurple}{\texttt{\#}}}
        \item To ignore a directory alongside with its contents, write {\textcolor{codepurple}{\texttt{dir\_name/}}}
        \begin{itemize}
            \item This is path agnostic. Meaning, anywhere that directory name is found, it is going to be ignored alongside with its contents.
            \item If you want a specific path, give a relative directory. e.g. {\textcolor{codepurple}{\texttt{/dir\_name/}}}
        \end{itemize}
        \item {\textcolor{codepurple}{\texttt{/}}} indicates thecurrent relative directory.
        \item Files don't have `/' at the end.
        \item To ignore Java files that have `.java' at the end, write {\textcolor{codepurple}{\texttt{*.java}}}
        \item To ignore C or C++ files, write {\textcolor{codepurple}{\texttt{*.c*}}}
        \item Extra spaces between lines don't affect.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Common Files (Cont'd) \hspace{4.2cm} INSTALL.md}\
    A Markdown file that contains instructions and guidelines on how to install and set up the software on various platforms.\\
    \vspace{10pt}
    It should contain the following sections:
    \begin{itemize}
        \item download packages
        \item clone repository (give the HTTP link)
        \item virtual environment setup
        \item miscellaneous (optional)
    \end{itemize}
    \vspace{10pt}
    You can check a sample \href{https://github.com/rootminusone8004/demorepo/blob/main/INSTALL.md}{\textcolor{linkblue}{here}}.

    Also check out the raw version to see how it is coded.\\
    \vspace{10pt}
    To learn basic coding in mark down language, follow \href{https://www.markdownguide.org/basic-syntax/}{\textcolor{linkblue}{this article}}.
\end{frame}

\begin{frame}
    \frametitle{Common Files (Cont'd) \hspace{4.2cm} README.md}\
    A Markdown file that contains a brief but concise description of the project. It also covers crucial information.\\
    \vspace{10pt}
    It should contain the following sections:
    \begin{itemize}
        \item brief heading
        \begin{itemize}
            \item \textbf{Main / Master}: project description
            \item \textbf{Other}: warning

            (Branches will be discussed soon)
        \end{itemize}
        \item important note (if exists)
        \item installation (navigate to INSTALL.md)
        \item execution
        \item License (main branch)
    \end{itemize}
    \vspace{10pt}
    Click \href{https://github.com/rootminusone8004/demorepo/blob/main/README.md}{\textcolor{linkblue}{here}} to see a sample in main branch. \\
    Click \href{https://github.com/rootminusone8004/demorepo/blob/kwoshik/README.md}{\textcolor{linkblue}{here}} to see a sample in other branch.

    Also check out the raw version to see how it is coded.
\end{frame}

\begin{frame}
    \frametitle{Git Branch}\
    A good repository has three basic branches.
    \begin{itemize}
        \item \textbf{Main / Master}
            \begin{itemize}
            \item This is the main software branch.
            \item Major software pushes are given here.
            \item Low development.
            \end{itemize}
        \item \textbf{Integration}
            \begin{itemize}
            \item Works are merged.
            \item Developer conflicts are removed.
            \item Stable code is ready for main branch.
            \end{itemize}
        \item \textbf{Development}
            \begin{itemize}
            \item High development
            \item Code are added, checked, removed etc.
            \item High bugs.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Git Branch(Cont'd)}\
    \begin{itemize}
        \item \textbf{Create new branch:}\\
        Initialize everything as usual. Just two things to follow:
        \begin{itemize}
            \item Naming the branch
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git branch -m kwoshik}
            \end{tcolorbox}
            \item First upload to remote repository
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git push --set-upstream origin kwoshik}
            \end{tcolorbox}
        \end{itemize}
        After that, you can manage everything via vscode.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Git Branch(Cont'd) \hspace{5.2cm} Main branch}\
    \begin{itemize}
        \item \textbf{Clone repository:}\\
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.9\textwidth]
            \texttt{git clone "git@github:rootminusone8004/demorepo"}
        \end{tcolorbox}
        \item \textbf{Commit message:}\\
        \begin{itemize}
            \item one line message, it's version number (e.g. v 4.8.1)
            \item First number is \textbf{major version}
            \item Second number is \textbf{minor version}
            \item Third number is \textbf{patch number}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Git Branch(Cont'd) \hspace{5.2cm} Main branch}\
    \begin{itemize}
        \item \textbf{CHANGELOG.md}\\
        \vspace{10pt}
        This file lists changes made to the project in each version release including new features and improvements.
        \vspace{5pt}
        \begin{itemize}
            \item All versions will be included in this file.
            \item project development tracker
        \end{itemize}
        \vspace{5pt}
        You can check a sample \href{https://github.com/rootminusone8004/demorepo/blob/main/CHANGELOG.md}{\textcolor{linkblue}{here}}.

        Also check out the raw version to see how it is coded.
        \vspace{10pt}
        \item \textbf{LICENSE.txt}\\
        \vspace{10pt}
        The main branch must have a license file. But other branches don't need it since all of them fall under the same license.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Git Branch(Cont'd) \hspace{5.2cm} Main branch}\
    \begin{itemize}
        \item \textbf{Upload the work:}\\
        \begin{itemize}
            \item check out the integration branch
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git checkout integration}
            \end{tcolorbox}
            \item pull the latest changes
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git pull origin integration}
            \end{tcolorbox}
            \item make sure the working directory is clean
            \item push the integration branch
            \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
                \texttt{git push origin integration:main --force}
            \end{tcolorbox}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \vskip -0.5cm
    \frametitle{Git Branch(Cont'd) \hspace{4cm} Integration branch}\
    \begin{itemize}
        \item \textbf{Clone repository:}\\
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.9\textwidth]
            \texttt{git clone -b integration "git@github:rootminusone8004/demorepo"}
        \end{tcolorbox}
        \item \textbf{Commit message:}\\
        \begin{itemize}
            \item multi line message
            \item each line indicates new implemented features
        \end{itemize}
        \begin{lstlisting}[style=notepad]
Email signups will now need authorization.
UI navigation is improved.
Camera tool added. 
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \vskip -0.5cm
    \frametitle{Git Branch(Cont'd) \hspace{4cm} Integration branch}\
    \begin{itemize}
        \item \textbf{TODO.md}\\
        \vspace{10pt}
        This file might contain a list of tasks or features that need to be implemented in the project.
        \vspace{5pt}
        \begin{itemize}
            \item An integration tracker file.
            \item Only bug fixes and miscellaneous sections should be here.
            \item `X' means job done and `-' means partial done.
        \end{itemize}
        \vspace{5pt}
        \begin{lstlisting}[style=notepad]
# TODO List

## Bug Fixes
- [X] Fixed the mobile.java integration
- [ ] Fixed the navbar section

## Miscellaneous
- [-] Update documentation for API endpoints
- [ ] successful apk build
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \vskip -0.5cm
    \frametitle{Git Branch(Cont'd) \hspace{3.5cm} Development branch}\
    \begin{itemize}
        \item \textbf{Clone repository:}\\
        \begin{tcolorbox}[colback=backcolour, colframe=bordercolour, width=0.8\textwidth]
            \begin{minipage}{\linewidth}
                git clone -b kwoshik \\
                "git@github:rootminusone8004/demorepo"
            \end{minipage}
        \end{tcolorbox}
        \item \textbf{Commit message:}\\
        \begin{itemize}
            \item four lines message
            \item First number is the concise summary of what happened
            \item The rest three lines answer three separate questions:
            \begin{itemize}
                \item What is now different than before?
                \item What’s the reason for the change?
                \item How is it improvement then the previous one?
            \end{itemize}
        \end{itemize}
        \begin{lstlisting}[style=notepad]
Email signups will now need authorization:
-- signup.php uses our captcha library
-- invalid signups will now get blocked
-- now it is impossible to access with credentials
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \vskip -0.5cm
    \frametitle{Git Branch(Cont'd) \hspace{3.5cm} Development branch}\
    \begin{itemize}
        \item \textbf{TODO.md}\\
        \vspace{10pt}
        \begin{itemize}
            \item Already discussed in integration branch
            \item Two more sections are needed: Features and Engancements
            \item `X' means job done and `-' means partial done.
        \end{itemize}
    \end{itemize}
    \vspace{10pt}
    You can check a sample \href{https://github.com/rootminusone8004/demorepo/blob/kwoshik/TODO.md}{\textcolor{linkblue}{here}}.

    Also check out the raw version to see how it is coded.
\end{frame}

\begin{frame}
    \frametitle{Thank You!}
    \centering
    \Huge
    Thanks for your attention. \\
    Any questions?
\end{frame}

\end{document}