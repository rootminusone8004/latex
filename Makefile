all:
	lualatex hum.tex
	
# biber hum
# lualatex hum.tex

# %:
# 	lualatex $@.tex
# 	biber $@
# 	lualatex $@.tex
# 	xreader $@.pdf

remove:
	rm -f hum.aux
	rm -f hum.bbl
	rm -f hum.bcf	
	rm -f hum.blg
	rm -f hum.lof
	rm -f hum.log
	rm -f hum.nav
	rm -f hum.out
	rm -f hum.pdf
	rm -f hum.run.xml
	rm -f hum.snm
	rm -f hum.toc
	rm -f hum.vrb